namespace DocumentStore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _201507211156564_201507210645253_InitialCreate : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Comments", "DocumentId", "dbo.Documents");
            DropIndex("dbo.Comments", new[] { "DocumentId" });
        }
        
        public override void Down()
        {
            CreateIndex("dbo.Comments", "DocumentId");
            AddForeignKey("dbo.Comments", "DocumentId", "dbo.Documents", "Id", cascadeDelete: true);
        }
    }
}
