﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using DocumentStore.Models;
using System.Web;
using System.IO;

namespace DocumentStore.Controllers
{
    public class DocumentsController : BaseApiController
    {
        private DocumentStoreContext db = new DocumentStoreContext();

        // GET api/Documents
        public IQueryable<Document> GetDocuments()
        {
            return db.Documents;
        }

        // GET api/Documents/5
        [ResponseType(typeof(Document))]
        public IHttpActionResult GetDocument(int id)
        {
            Document document = db.Documents.Find(id);
            if (document == null)
            {
                return NotFound();
            }

            return Ok(document);
        }

        // PUT api/Documents/5
        public IHttpActionResult PutDocument(int id, Document document)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != document.Id)
            {
                return BadRequest();
            }

            db.Entry(document).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DocumentExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/Documents
        [ResponseType(typeof(Document))]
        public IHttpActionResult PostDocument(Document document)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Documents.Add(document);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = document.Id }, document);
        }

        [HttpPost]
        [HttpOptions]
        [Route("api/Document/Upload")]
        public IHttpActionResult UploadDocument()
        {
            string filePath = null;
            var httpRequest = HttpContext.Current.Request;

            var documentType = httpRequest.QueryString["type"];
            var path = string.Empty;

            if (httpRequest.Files.Count > 0)
            {
                string docfiles = null;
                foreach (string file in httpRequest.Files)
                {
                    var postedFile = httpRequest.Files[file];
                    //string taskWorkingDirectory = Server.MapPath("Abhimanyu Kumar Vatsa");
                    var mappedPath = System.Web.Hosting.HostingEnvironment.MapPath("~/UploadedFiles");
                    var documentTypePath = mappedPath + "/" + documentType;
                    if (Directory.Exists(documentTypePath))
                    {
                        filePath = documentTypePath + "/" + postedFile.FileName;
                    }
                    else
                    {
                        Directory.CreateDirectory(documentTypePath);
                        filePath = documentTypePath + "/" + postedFile.FileName;
                    }

                    path = filePath;
                    postedFile.SaveAs(filePath);
                    docfiles = filePath;
                }

            }

            return Ok(path);
        }

        // DELETE api/Documents/5
        [ResponseType(typeof(Document))]
        public IHttpActionResult DeleteDocument(int id)
        {
            Document document = db.Documents.Find(id);
            if (document == null)
            {
                return NotFound();
            }

            db.Documents.Remove(document);
            db.SaveChanges();

            return Ok(document);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DocumentExists(int id)
        {
            return db.Documents.Count(e => e.Id == id) > 0;
        }
    }
}