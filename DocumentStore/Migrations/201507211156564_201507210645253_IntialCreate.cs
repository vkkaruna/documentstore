namespace DocumentStore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _201507210645253_IntialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DocumentId = c.Int(nullable: false),
                        Comments = c.String(),
                        Updated_Date = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Documents", t => t.DocumentId, cascadeDelete: true)
                .Index(t => t.DocumentId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Comments", "DocumentId", "dbo.Documents");
            DropIndex("dbo.Comments", new[] { "DocumentId" });
            DropTable("dbo.Comments");
        }
    }
}
